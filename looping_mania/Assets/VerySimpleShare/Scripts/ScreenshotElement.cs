﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

#if VS_SHARE
namespace AppAdvisory.SharingSystem
{
	public class ScreenshotElement : MonoBehaviour
	{
		public CanvasGroup parent;
		public Button button;
		public Image image;
		public Text text;

		[NonSerialized] 
		public Vector2 defaultLocalPos;
		[NonSerialized] 
		public Quaternion defaultRotation;
		[NonSerialized] 
		public Vector2 defaultSizeDelta;

		RectTransform _parentRectTransform;
		public RectTransform parentRectTransform
		{
			get
			{
				if(_parentRectTransform == null)
					_parentRectTransform = parent.GetComponent<RectTransform>();

				return _parentRectTransform;
			}
		}
	}
}
#endif